            //移動制限用
            double oldOffsetX = matrix.OffsetX;
            double oldOffsetY = matrix.OffsetY;

            // 
            // 拡大用のマトリクスを算出
            // 移動した後の補正は別でやる
            matrix.ScaleAt(scale, scale, image.ActualWidth / 2.0, image.ActualHeight / 2.0);


            // 移動開始点と現在位置の差から、MouseMoveイベント1回分の移動量を算出
            double offsetX = matrix.OffsetX;
            double offsetY = matrix.OffsetY;

            if (Math.Abs(offsetX) >= elm.ActualWidth * matrix.M11)
            {
                matrix.Translate(oldOffsetX, );
            }
            if (Math.Abs(offsetY) >= elm.ActualHeight * matrix.M11)
            {
                matrix.Translate(oldOffsetX, oldOffsetY);
            }
            
            
            
            // TranslateメソッドにX方向とY方向の移動量を渡し、移動後の状態を計算
            Matrix tmpMat = matrix;
            tmpMat.Translate(offsetX, offsetY);
            if (Math.Abs(tmpMat.OffsetX) >= scrV.ActualWidth*tmpMat.M11)
            {
                offsetX = 0;
            }
            if (Math.Abs(tmpMat.OffsetY) >= scrV.ActualHeight * tmpMat.M11)
            {
                offsetY = 0;
            }
            matrix.Translate(offsetX, offsetY);